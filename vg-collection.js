//define a class with three parameters in the constructor
class videoGame {
    constructor(name, platform, year)
    {
        this._name = name
        this._platform = platform
        this._year = year
    }

    //getters for each of the specified properties
    get name() {return this._name}
    get platform() {return this._platform}
    get year() {return this._year}

    //setter for year parameter
    set year(number) {
        //if statement to check the the format (using typeof is a number)
        if (typeof number === 'number')
        {
            this._year = number
        }
        //if input is not a number, return a error message
        else
        {
            return 'Input is in the wrong format!'
        }
    }

    //method to display
    displayGames() {
        console.log(`Name : ${this._name}`)
        console.log(`Platform : ${this._platform}`)
        console.log(`Year of release : ${this._year}\n`)
    }
}

//class inherits from videoGame class
class gameConsole extends videoGame {
    constructor(name, year, manufacturer, controllerPorts)
    {
        //super should always be declared before anything else in the constructor
        //remember - all parameters from the parent class need to be defined in super, even if you give them a default value, like Console!
        super(name, 'Console', year)
        this._manufacturer = manufacturer
        this._controllerPorts = controllerPorts
    }

    //getters for the new parameters in the gameConsole class
    get manufacturer() {return this._manufacturer}
    get controllerPorts() {return this._controllerPorts}

    //setter for the controllerPorts parameter, checks if it is a number
    set controllerPorts(number) {
        if (typeof number === "number")
        {
            this._controllerPorts = number
        }
        else
        {
            return 'Input is in the wrong format!'
        }
    }

    //method to display
    displayConsole() {
        console.log(`Name: ${this._name}`)
        console.log(`Year : ${this._year}`)
        console.log(`Manufacturer : ${this._manufacturer}`)
        console.log(`Number of controller ports : ${this._controllerPorts}\n`)
    }
}

//greeting message
console.log("Welcome to your Video Game Database! \nLet's view some games!\n")
//creating a new object of the videoGame class
const waveRave64 = new videoGame ('Wave Race 64', 'Nintendo 64', 1996)
const tacticsOgre = new videoGame ('Tactics Ogre: Let us cling together', 'Super Nintendo Entertainment System', 1995)
const elevatorActionReturns = new videoGame ('Elevator Action Returns', 'Sega Saturn', 1997)

//calling the method to display games
waveRave64.displayGames()
tacticsOgre.displayGames()
elevatorActionReturns.displayGames()
console.log("\nLet's view some consoles!\n")

//creating a new object of the gameConsole class
const superNintendo = new gameConsole ('Super Nintendo Entertainment System', 1992, "Nintendo", 2)
const segaSaturn = new gameConsole ('Sega Saturn', 1994, 'Sega', 2)
const nintendo64 = new gameConsole ('Nintendo 64', 1996, 'Nintendo', 4)

//calling the method to display game console
superNintendo.displayConsole()
segaSaturn.displayConsole()
nintendo64.displayGames()
